package Actions;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import StartBrowser.Start;

public class SnapShot {
	
	public static void takeScreenShot(String name) throws Exception {
		TakesScreenshot sc = (TakesScreenshot) Start.getDriver();
		File arquivo = sc.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File(name));
		
	}
}
