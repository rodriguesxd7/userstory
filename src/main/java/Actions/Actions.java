package Actions;

import static StartBrowser.Start.getDriver;
import static org.openqa.selenium.By.xpath;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class Actions {

	//Click on generic link
	public void clickLink(String singIn){
		getDriver().findElement(By.linkText(singIn)).click();
	}
	
	//Click on generic button
	public void clickButton(String singIn){
		getDriver().findElement(xpath(singIn)).click();
	}

	//Enter email
	public void sendEmail(String email){
		getDriver().findElement(By.id("user_email")).sendKeys(email);
	}
	
	//Enter password
	public void sendPassword(String password){
		getDriver().findElement(By.id("user_password")).sendKeys(password);
	}

	//Text verify
	public void textVerify(String text1){
		WebElement text = getDriver().findElement(By.linkText(text1));
		text.getText();
		/*System.out.println(text1);
		Assert.assertEquals(text1, text);
		System.out.println("Expected text: " + text1 + " - Received text: " + text);*/
	}
	
	public void enterForm(String name, String task){
		WebElement enterText = getDriver().findElement(By.name(name));
		enterText.click();
		enterText.sendKeys(task);
	}
	
	public void pressEnter(){
		WebElement textbox = getDriver().findElement(By.id("new_task"));
		textbox.sendKeys(Keys.ENTER);
	}
	
	public void enterDate(String dueDate){
		getDriver().findElement(By.id("dueDate")).sendKeys(dueDate);
	}
	
	
}
