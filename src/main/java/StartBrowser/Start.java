package StartBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Start {

	private static WebDriver driver;

	public static WebDriver getDriver() {
		if(driver == null) {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\brunocrodrigues\\Downloads\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://qa-test.avenuecode.com/");

			System.out.println(driver.getTitle());

		}
		return driver;

	}

	public void closeBrowser(){
		driver.close();

	}
}
