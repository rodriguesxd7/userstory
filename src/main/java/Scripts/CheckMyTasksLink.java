package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;
import Actions.SnapShot;
import StartBrowser.Start;

public class CheckMyTasksLink {

	@Test
	public void validateLinkMyTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		SnapShot.takeScreenShot("Tela inicial.jpg");
		act.textVerify("My Tasks");
		//SnapShot.takeScreenShot("Verify My Tasks Link.jpg");
		//act.clickLink("My Tasks");
		
		Start.getDriver().close();
	}

}
