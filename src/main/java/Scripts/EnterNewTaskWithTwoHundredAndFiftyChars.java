package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;

public class EnterNewTaskWithTwoHundredAndFiftyChars {
	
	@Test
	public void enterTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		act.textVerify("My Tasks");
		act.clickLink("My Tasks");
		act.enterForm("new_task", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has abcdefghijklm");		
		act.clickButton("/html/body/div[1]/div[2]/div[1]/form/div[2]/span");
		act.textVerify("AB");
		
		//Start.getDriver().close();
	}


}
