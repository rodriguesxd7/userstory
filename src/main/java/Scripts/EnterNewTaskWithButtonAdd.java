package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;
import StartBrowser.Start;

public class EnterNewTaskWithButtonAdd {
	
	@Test
	public void enterTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		act.textVerify("My Tasks");
		act.clickLink("My Tasks");
		act.enterForm("new_task", "Exemple tasks of user story");		
		act.clickButton("/html/body/div[1]/div[2]/div[1]/form/div[2]/span");
		act.textVerify("Exemple tasks of user story");
		
		//Start.getDriver().close();
	}

}
