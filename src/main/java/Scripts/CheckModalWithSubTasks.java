package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;
import StartBrowser.Start;

public class CheckModalWithSubTasks {
	
	@Test
	public void enterTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		act.textVerify("My Tasks");
		act.clickLink("My Tasks");
		act.clickButton("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button");
		act.textVerify("subtask exemple");
		act.clickButton("/html/body/div[4]/div/div/div[3]/button");
		
		Start.getDriver().close();
	}

}
