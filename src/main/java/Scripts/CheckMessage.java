package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;

public class CheckMessage {
	
	@Test
	public void validateLinkMyTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		act.textVerify("My Tasks");
		act.clickLink("My Tasks");
		act.textVerify("Hey Bruno, this is your todo list for today:");
		
		finalize();
	}


}
