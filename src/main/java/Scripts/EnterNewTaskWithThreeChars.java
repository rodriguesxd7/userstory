package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;

public class EnterNewTaskWithThreeChars {
	
	@Test
	public void enterTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		act.textVerify("My Tasks");
		act.clickLink("My Tasks");
		act.enterForm("new_task", "AB");		
		act.clickButton("/html/body/div[1]/div[2]/div[1]/form/div[2]/span");
		act.textVerify("AB");
		
		//Start.getDriver().close();
	}


}
