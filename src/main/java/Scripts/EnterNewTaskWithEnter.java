package Scripts;

import org.junit.Test;

import Actions.Actions;
import Actions.LoginPage;

public class EnterNewTaskWithEnter {
	
	@Test
	public void enterTasks() throws Throwable {
		
		LoginPage page = new LoginPage();
		Actions act = new Actions();
		
		page.realizarLogin();
		act.textVerify("My Tasks");
		act.clickLink("My Tasks");
		act.enterForm("new_task", "Exemple tasks of user story");		
		act.pressEnter();
		act.textVerify("Exemple tasks of user story");
		
		//Start.getDriver().close();
	}

}
