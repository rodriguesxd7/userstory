Feature: Check My Tasks Link on Nav Bar

  @tag1
  Scenario: Check My Tasks Link on Nav Bar and click
    Given That i'm logged in on the ToDo App
    And I want to check if the link 'My Tasks' is on the nav bar
    When I click in this link 
    And The page will be redirect to the page My Tasks
    Then i'll validate this page
    

  @tag2
  Scenario: Check the message 
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    Then I verify the message above of the new tasks bar
    
  @tag3  
  Scenario: Enter a new task hitting Enter or click on the add task 
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    And I will add a new task 
    Then I press enter to add the new task
      
  @tag4  
  Scenario: Enter a new task hitting Enter or click on the add task 
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    And I will add a new task 
    Then I click on the button plus to add the new task
    
  @tag5  
  Scenario: Enter a new task with than less 3 characters
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    And I will add a new task with than less three characters
    Then the system will refuse
    
  @tag6  
  Scenario: Enter a new task with more than 250 characters
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    And I will add a new task with more than 250 characters
    Then the system will refuse    
    
    
  @tag7  
  Scenario: Create a new subtask 
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    And I will add a new subtask clicking on the button Manage subtasks
    And write the description of the new subtasks
    And choosing a due date
    And click on the button Add
    Then the system will add a new subtasks 
    
   @tag8
   Scenario: Check if the new subtask is on the modal of subtasks
    Given That i'm logged in on the ToDo App
    When I click on link My Tasks 
    And I will check if my subtasks is it on the modal of subtask clicking on the button Manage subtasks
    Then i'll validate de subtasks 
